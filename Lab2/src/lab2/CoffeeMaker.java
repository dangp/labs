/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

/**
 *
 * @author HaiPhan
 */
public class CoffeeMaker {

    private boolean powerOn;
    private int waterTank;
    private int beansTank;
    private int timesOfUse;

    public CoffeeMaker() {
        this.powerOn = false;
        this.waterTank = 500;
        this.beansTank = 50;
        this.timesOfUse = 0;
    }

    public void turnOnOff() {
        this.powerOn = !this.powerOn;
        if (this.powerOn) {
            System.out.println("Machine is ON");
        } else {
            System.out.println("Machine is OFF");
        }
        rinse();
    }

    public void espressco() {
        if (powerOn) {
            if (this.waterTank < 30) {
                System.out.println("Not enough water. Please fill the water tank.");
            } else if (this.beansTank < 8) {
                System.out.println("Not enough beans. Please fill the beans tank.");
            } else {
                System.out.println("Enjoy your espressco.");
                this.waterTank -= 30;
                this.beansTank -= 8;
                this.timesOfUse++;
                System.out.println("Water tank: "+this.waterTank);
                System.out.println("Beans tank: "+this.beansTank);
                clean();
            }
        }
    }

    public void coffee() {
        if (powerOn) {
            if (this.waterTank < 150) {
                System.out.println("Not enough water. Please fill the water tank.");
            } else if (this.beansTank < 10) {
                System.out.println("Not enough beans. Please fill the beans tank.");
            } else {
                System.out.println("Enjoy your coffee.");
                this.waterTank -= 150;
                this.beansTank -= 10;
                this.timesOfUse++;
                System.out.println("Water tank: "+this.waterTank);
                System.out.println("Beans tank: "+this.beansTank);
                clean();
            }
        }
    }

    public void fillWater() {
        if (this.powerOn) {
            this.waterTank = 1000;
            System.out.println("Water tank filled");
        }
    }

    public void fillBeans() {
        if (this.powerOn) {
            this.beansTank = 100;
            System.out.println("Beans tank filled");
        }
    }

    public void rinse() {
        if (this.waterTank >= 10) {
            System.out.println("Rinsing...DONE");
            this.waterTank -= 10;
        } else {
            System.out.println("Not enough water to rinse...");
        }
    }

    public void clean() {
        if (this.timesOfUse % 5 == 0) {
            if (this.waterTank >= 300) {
                System.out.println("Cleaning after 5 times of use....");
                this.waterTank -= 300;
            } else {
                System.out.println("Not enought water to clean...");
            }
        }

    }
}
