/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab2;

import java.util.Scanner;

/**
 *
 * @author HaiPhan
 */
public class Lab2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CoffeeMaker cm = new CoffeeMaker(); // new coffee maker with full water tank and beans tank
        Scanner reader = new Scanner(System.in);
        System.out.println("Type 1: Turn ON/OFF \nType 2: Get espressco\nType 3: Get coffee\nType 4: Fill water\nType 5: Fill beans");
        while (true) {
            int input = Integer.parseInt(reader.nextLine());
            switch (input) {
                case 1:
                    cm.turnOnOff();
                    break;
                case 2:
                    cm.espressco();
                    break;
                case 3:
                    cm.coffee();
                    break;
                case 4:
                    cm.fillWater();
                    break;
                case 5:
                    cm.fillBeans();
            }
        }
    }
    
}
