/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author HaiPhan
 */
public class DiscTreeSet {
    private TreeSet<Disc> set;
    
    public DiscTreeSet() {
        this.set = new TreeSet<>();
    }

    public TreeSet<Disc> getTreeSet() {
        return set;
    }
    
    public void add(Disc cd) {
        this.set.add(cd);
    }
}
