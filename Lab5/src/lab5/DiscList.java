/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author HaiPhan
 */
public class DiscList {
    private List<Disc> list;
    
    public DiscList() {
        this.list = new ArrayList<>();
    }

    public List<Disc> getList() {
        return list;
    }
   
    public void add(Disc cd) {
        if (!this.list.contains(cd)) {
            this.list.add(cd);
        }
    }
}
