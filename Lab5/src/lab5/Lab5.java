/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author HaiPhan
 */
public class Lab5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        DiscList list = new DiscList();
        while (list.getList().size() < 100) {
            list.add(new Disc());
        }
        Collections.sort(list.getList());
        System.out.println("Sorted list: ");
        for (Disc a : list.getList()) {
            System.out.println(a);
        }

        DiscSet set = new DiscSet();
        while (set.getSet().size() < 100) {
            set.add(new Disc());
        }
        List sortedSet = new ArrayList(set.getSet());
        Collections.sort(sortedSet);
        System.out.println("Sorted set: ");
        for (Object a: sortedSet) {
            System.out.println(a);
        }
      
        DiscTreeSet treeSet = new DiscTreeSet();
        while (treeSet.getTreeSet().size() < 100) {
            treeSet.add(new Disc());
        }
        List sortedTreeSet = new ArrayList(treeSet.getTreeSet());
        Collections.sort(sortedTreeSet);
        System.out.println("Sorted TreeSet: ");
        for (Object a : treeSet.getTreeSet()) {
            System.out.println(a);
        }

    }

}
