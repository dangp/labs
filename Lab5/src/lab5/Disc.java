/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.Random;

/**
 *
 * @author HaiPhan
 */
public class Disc implements Comparable<Disc> {
    private String name;
   
    public Disc() {
        this.name = this.discName();
    }
    
    private String discName() {
        Random random = new Random();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        int cdNameLength = random.nextInt(20) + 1;
        String nameReturn = "";
        
        for (int i = 0; i < cdNameLength; i++) {
            char c = alphabet.charAt(random.nextInt(26));
            nameReturn = nameReturn + c;
        }
        return nameReturn;
    }
    
    public String toString()    {
        return this.name;
    }

    @Override
    public int compareTo(Disc t) {
        return this.name.compareTo(t.name);
    }
}
