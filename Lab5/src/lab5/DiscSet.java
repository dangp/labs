/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author HaiPhan
 */
public class DiscSet {
    private Set<Disc> hashSet;
    
    public DiscSet() {
        this.hashSet = new HashSet<>();
    }

    public Set<Disc> getSet() {
        return hashSet;
    }
    
    public void add(Disc cd) {
        if (!this.hashSet.contains(cd)) {
            this.hashSet.add(cd);
        }
    }
}
