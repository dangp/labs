/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

import java.util.ArrayList;

/**
 *
 * @author HaiPhan
 */
public class Disc {
    private ArrayList<String> trackList = new ArrayList<String>();
    private String discName;
    
    public Disc(String name, int number) {
        this.discName = name;
        int i=1;
        while (i<=number) {
            trackList.add("Track: " + i);
            i++;
        }
    }
    
    public int trackListNumber() {
        return trackList.size();
    }
    
    public ArrayList<String> getTrackList() {
        return this.trackList;
    }
    
    public String getSong(int index) {
        return trackList.get(index);
    }
        
    public String getDiscName() {
        return this.discName;
    }
}
