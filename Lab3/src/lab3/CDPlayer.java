/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HaiPhan
 */
public class CDPlayer implements Runnable {

    private boolean isPowerOn, isTrayOpen, isPlaying, trackPlaying;
    private int volume;
    private int track;
    private Disc disc;
    Scanner reader = new Scanner(System.in);

    ;

    public CDPlayer() {
        isPowerOn = false;
        isTrayOpen = false;
        isPlaying = false;
        trackPlaying = false;
        this.disc = null;
        this.volume = 5;
    }

    public void turnOnOff() {
        isPowerOn = !isPowerOn;
        if (isPowerOn) {
            System.out.println("CD Player is ON.");
        } else {
            stop();
            System.out.println("CD Player is OFF.");
        }
    }

    public boolean isEmpty() {
        return this.disc == null;
    }

    public void next() {
        track++;
    }

    public void stop() {
        trackPlaying = false;
    }

    public void upVolume() {
        if (isPowerOn) {
            if (volume < 20) {
                volume++;
            }
            System.out.println("Volume: " + this.volume);
        }
    }

    public void downVolume() {
        if (isPowerOn) {
            if (volume > 0) {
                volume--;
            }
            System.out.println("Volume: " + this.volume);
        }
    }

    public void openCloseTray(Disc disc) {
        if (isPowerOn) {
            stop();
            if (!isTrayOpen) {
                isTrayOpen = true;
                isPlaying = false;
                if (isEmpty()) {
                    insertDisc(disc);
                } else {
                    removeDisc(disc);
                }
            }
            isTrayOpen = false;
            return;
        }
        System.out.println("CD Player is OFF.");
    }

    public void insertDisc(Disc disc) {
        if (isPowerOn) {
            if (isTrayOpen) {
                this.disc = disc;
                System.out.println(disc.getDiscName() + " inserted.");
            } else {
                System.out.println("The tray is closed.");
            }
        }
    }

    public void removeDisc(Disc disc) {
        if (isPowerOn) {
            if (isTrayOpen) {
                this.disc = null;
                System.out.println(disc.getDiscName() + " removed.");
            } else {
                System.out.println("The tray is closed.");
            }
        }
    }

    public void play() {
        if (isPowerOn) {
            if (!isTrayOpen) {
                if (!isEmpty()) {
                    trackPlaying = true;
                    Thread t = new Thread(this);
                    t.start();
                } else {
                    System.out.println("No disc!!!");
                }
            } else {
                System.out.println("Please close the disc tray.");
            }
        }
    }

    @Override
    public void run() {
        while (trackPlaying) {
            track = 0;
            while (track < disc.getTrackList().size()) {
                if (!trackPlaying) {
                    break;
                }
                System.out.println(disc.getSong(track) + " is playing...");
                try {
                    Thread.sleep(2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                track++;
            }
            break;
        }
    }
}
