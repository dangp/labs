/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

import java.util.Scanner;

/**
 *
 * @author HaiPhan
 */
public class Lab3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        CDPlayer cdPlayer = new CDPlayer();
        Disc disc1 = new Disc("Disc 1", 10);
        Scanner reader = new Scanner(System.in);
        System.out.println("Type 1: Turn ON/OFF \nType 2: Insert/remove disc\nType 3: Play\nType 4: Next\nType 5: Stop\nType 6: Down volume\nType 7: Up volume");
        while (true) {
            int input = Integer.parseInt(reader.nextLine());
            switch (input)  {
                case 1: 
                    cdPlayer.turnOnOff();
                    break;
                case 2:
                    cdPlayer.openCloseTray(disc1);
                    break;
                case 3: 
                    cdPlayer.play();
                    break;
                case 4:
                    cdPlayer.next();
                    break;
                case 5:
                    cdPlayer.stop();
                    break;
                case 6:
                    cdPlayer.downVolume();
                    break;
                case 7:
                    cdPlayer.upVolume();
                    break;
            }
        }
    }

}
