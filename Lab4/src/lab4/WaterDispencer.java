/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HaiPhan
 */
public class WaterDispencer implements Runnable {

    private boolean powerOn;
    private boolean lidOpen;
    private int waterLevel;
    private boolean valveOpen;

    public WaterDispencer() {
        this.waterLevel = 10;
        this.powerOn = false;
        this.lidOpen = false;
    }

    public void turnOnOff() {           //turn the machine ON/OFF
        powerOn = !powerOn;
        if (powerOn) {
            System.out.println("The machine is ON");
        } else {
            System.out.println("The machine is OFF");
        }
    }

    public void coverOnOff() {          //open/close the lid
        if (powerOn) {
            lidOpen = !lidOpen;
            if (lidOpen) {
                System.out.println("Lid is open");
            } else {
                System.out.println("Lid is close");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void fillTank() {          //if the lid is open then fill the tank, if not => do nothing
        if (powerOn) {
            if (lidOpen) {
                waterLevel = 10;
                System.out.println("DONE. Water level is " + waterLevel);
            } else {
                System.out.println("Please open the lid");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void getWater() {          //if the machine is ON, serve water if not then nothing happens
        if (powerOn) {
            if (waterLevel > 0) {
                waterLevel--;
                System.out.println("Enjoy the water!!!");
                System.out.println("Water Level: " + waterLevel);
            } else {
                System.out.println("Please fill the tank");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void getWater(int amount) {
        if (powerOn) {
            if (waterLevel - amount >= 0) {
                waterLevel -= amount;
                System.out.println("Enjoy!!!");
                System.out.println("Water Level: " + waterLevel);
            } else {
                System.out.println("Please fill the tank");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void pressWaterButton() {
        if (powerOn) {
            this.valveOpen = true;
            Thread t = new Thread(this);
            t.start();
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void unpressWaterButton() {
        this.valveOpen = false;
    }

    @Override
    public void run() {
        System.out.println("Pouring....");
        while (valveOpen) {
            while (waterLevel > 0 && valveOpen) {
                getWater();
                System.out.println("*****");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(WaterDispencer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;
        }
    }
}
