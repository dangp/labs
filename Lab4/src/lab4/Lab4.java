/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.Scanner;

/**
 *
 * @author HaiPhan
 */
public class Lab4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        WaterDispencer Lavie = new WaterDispencer();
        Scanner reader = new Scanner(System.in);
        System.out.println("Type 1: Turn ON/OFF \nType 2: Open/close the lid\nType 3: Fill tank\nType 4: Get water\nType 5: Get more water\nType 6: Press water button\nType 7: Unpress water button");
        while (true) {
            int input = Integer.parseInt(reader.nextLine());
            switch (input) {
                case 1:
                    Lavie.turnOnOff();
                    break;
                case 2:
                    Lavie.coverOnOff();
                    break;
                case 3:
                    Lavie.fillTank();
                    break;
                case 4:
                    Lavie.getWater();
                    break;
                case 5:
                    System.out.println("Amout: ");
                    int amount = Integer.parseInt(reader.nextLine());
                    Lavie.getWater(amount);
                case 6:
                    Lavie.pressWaterButton();
                    break;
                case 7:
                    Lavie.unpressWaterButton();
                    break;
            }
        }
    }
    
}
