/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

/**
 *
 * @author HaiPhan
 */
public class WaterDispencer {

    private boolean powerOn;
    private boolean lidOpen;
    private int waterLevel;

    public WaterDispencer() {
        this.waterLevel = 10;
        this.powerOn = false;
        this.lidOpen = false;
    }

    public void turnOnOff() {           //turn the machine ON/OFF
        powerOn = !powerOn;
        if (powerOn) {
            System.out.println("The machine is ON");
        } else {
            System.out.println("The machine is OFF");
        }
    }

    public void coverOnOff() {          //open/close the lid
        if (powerOn) {
            lidOpen = !lidOpen;
            if (lidOpen) {
                System.out.println("Lid is open");
            } else {
                System.out.println("Lid is close");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void fillTank() {          //if the lid is open then fill the tank, if not => do nothing
        if (powerOn) {
            if (lidOpen) {
                waterLevel = 10;
                System.out.println("DONE. Water level is " + waterLevel);
            } else {
                System.out.println("Please open the lid");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }

    public void getWater() {          //if the machine is ON, serve water if not then nothing happens
        if (powerOn) {
            if (waterLevel > 0) {
                waterLevel--;
                System.out.println("Enjoy the water!!!");
                System.out.println("Water Level: " + waterLevel);
            } else {
                System.out.println("Please fill the tank");
            }
        } else {
            System.out.println("Please turn the machine ON");
        }
    }
}
