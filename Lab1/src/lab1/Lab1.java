/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab1;

import java.util.Scanner;

/**
 *
 * @author HaiPhan
 */
public class Lab1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        WaterDispencer Lavie = new WaterDispencer();
        Scanner reader = new Scanner(System.in);
        System.out.println("Type 1: Turn ON/OFF \nType 2: Open/close the lid\nType 3: Fill tank\nType 4: Get water");
        while (true) {
            int input = Integer.parseInt(reader.nextLine());
            switch (input) {
                case 1:
                    Lavie.turnOnOff();
                    break;
                case 2:
                    Lavie.coverOnOff();
                    break;
                case 3:
                    Lavie.fillTank();
                    break;
                case 4:
                    Lavie.getWater();
                    break;
            }
        }

    }
}
